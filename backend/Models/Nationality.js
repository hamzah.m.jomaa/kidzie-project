const { Schema, model } = require("mongoose")

const schema = Schema({
    title:{
        type:String,
        required: [true,"Nationality is Required"]
    }
})

module.exports = model("nationality",schema)
