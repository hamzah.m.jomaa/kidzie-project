const Nationality = require("../Models/Nationality")

exports.getData = async (req,res) =>{
    const nationality = await Nationality.find({})
    return res.status(200).json({
        status:200,
        data:{
            nationality
        }
    })
}