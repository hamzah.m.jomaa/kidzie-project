const Auth = require("./Auth")
const getRequests = require("./getRequests")

module.exports.Router = (app) =>{
    app.use("/api/",getRequests)
    app.use("/api/user",Auth)
}